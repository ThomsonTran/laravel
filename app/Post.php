<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Post extends Model
{
    // Table Name
    protected $table = 'posts';
    // PK
    public $primaryKey = 'id';

    //timestamps
    public $timestamps = true;

    protected $fillable = ['title', 'body'];
}
