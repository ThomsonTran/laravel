@extends('layouts/app')

@section('content')
 
 
    <div class="card uper">
        <div class="card-header">
          <h3>Create Post</h3>
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('posts.store') }}">
                <div class="form-group">
                    @csrf
                    <label for="title">Title:</label>
                    <input type="text" class="form-control" name="title"/>
                </div>
                <div class="form-group">
                    <label for="body">Body:</label>
                    <textarea class="form-control" id="body" name='body' rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Post</button>
            </form>
        </div>
      </div>

@endsection