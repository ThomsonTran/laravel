
    <nav class="navbar navbar-expand-md navbar-dark bg-dark mb-5">
        <div class ="container">
         <a class="navbar-brand " href="#">{{config('app.name', 'LSAPP')}}</a>
      
            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                  <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/about">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/services">Services</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/posts">Posts</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
              </ul>

              <ul class="nav navbar-nav navbar-right">
                  <li class="nav-item">
                      <a class="nav-link" href="/posts/create">Create Posts</a>
                  </li>
              </ul>
            </div>
        </div>
    </nav>
      